import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/home.css";

export default function Home() {
  const history = useHistory();
  const [menu, setMenu] = useState([]);
  const [pages, setPages] = useState(0);
  const [nama, setNama] = useState("");

  const addToCart = async (item) => {
    console.log(item);
    await axios.post("http://localhost:3008/cart/", {
      produckId: item.id,
      quantity: item.quantity,
      userId: localStorage.getItem("id"),
    });
    Swal.fire({
      icon: "success",
      title: "Berhasil di masukkan ke cart",
      timer: 5000,
      buttons: false,
    })
      .then(() => {
        history.push("/home");
      })
      .catch((error) => {
        alert("Terjadinya kesalahan " + error);
      });
  };

  const getAll = async (idx) => {
    await axios
      .get(`http://localhost:3008/produck/all-produck?nama=${nama}&page=` + idx)
      .then((res) => {
        setPages(res.data.data.totalPages);
        setMenu(
          res.data.data.content.map((item) => ({
            ...item,
            quantity: 1,
          }))
        );
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const plusQuantity = (idx) => {
    const produck = [...menu];
    if (produck[idx].quantity < 45) {
      produck[idx].quantity++;
      setMenu(produck);
    }
  };
  const menQuantity = (idx) => {
    const produck = [...menu];
    if (produck[idx].quantity > 1) {
      produck[idx].quantity--;
      setMenu(produck);
    }
  };
  return (
    <div className="hom">
      <div
        id="carouselExampleControls"
        className="carousel slide"
        data-bs-ride="carousel"
        style={{}}
      >
        {/* <div className="nemu">
          <h1 className="B">
            <b>Welcome to my shop</b>
          </h1>{" "}
          <hr />
          
        </div> */}
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              src="https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2017/10/banner-tokopedia-2.jpg"
              className="d-block w-100"
              style={{ width: 50 }}
              alt="..."
            />
            {/* <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus dolore temporibus numquam tenetur, id eligendi tempora eos hic saepe! Reiciendis, aut recusandae. Qui rerum dolorem natus eveniet fuga explicabo culpa!</p> */}
          </div>
          <div className="carousel-item">
            {/* <div style={{display: "flex", gap: 100, padding:20}}> */}
            <img
              src="https://b.zmtcdn.com/data/pictures/chains/9/18573599/ae6be06683994121281073b1daee5542.jpg"
              className="d-block w-100"
              alt="..."
            />
            {/* <div style={{marginTop: 80}}>
              <h1>Steak</h1>
            <p>Steik adalah sepotong besar daging, biasanya daging sapi. Daging merah, dada ayam, dan ikan sering kali dipotong menjadi steik. Kebanyakan steik dipotong tegak lurus dengan serat otot, menambah kelegitan daging.</p>
            </div> */}
            {/* </div> */}
          </div>
          <div className="carousel-item">
            <img
              src="https://coffeesite.kz/wp-content/uploads/2021/06/McCafe.jpg"
              className="d-block w-100"
              style={{ width: 50 }}
              alt="..."
            />
          </div>
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleControls"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleControls"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
      <hr style={{ backgroundColor: "green", height: 5 }} />
      <h2>Ada beberapa menu di sini</h2>
      <hr style={{ backgroundColor: "green", height: 5 }} />

      <div className="irr">
        {/* <h2 class="nem">Daftar Menu</h2> */}
        <form class="d-flex" role="search">
          <input
            class="form-control me-2"
            type="search"
            value={nama}
            onChange={(e) => setNama(e.target.value)}
            placeholder="Search"
            aria-label="Search"
          />
          <button
            class="btn btn-outline-success"
            type="button"
            onClick={() => getAll(0)}
          >
            Search
          </button>
        </form>

        {menu.length == 0 ? (
          <h4 align="center">produck tidak tersedia</h4>
        ) : (
          <div
            style={{ padding: 10, paddingTop: 30, display: "flex", gap: 30 }}
            className="flex-wrap"
          >
            {menu.map((daftar, idx) => (
              <div
                class="card"
                style={{
                  width: "14rem",
                  height: "25rem",
                  backgroundColor: "palegoldenrod",
                  padding: 3,
                }}
              >
                <img
                  style={{ width: 217, height: 200 }}
                  src={daftar.img}
                  class="card-img-top"
                  alt="..."
                />
                <div class="card-body" style={{ textAlign: "center" }}>
                  <h5 class="card-title">{daftar.nama}</h5>
                  <p class="card-text">{daftar.deskripsi}</p>
                  <div className="menn">
                    <h6>{daftar.harga}</h6>
                    <h6 className="oton">
                      {daftar.quantity == 1 ? (
                        <button
                          className="grey"
                          onClick={() => menQuantity(idx)}
                        >
                          -{" "}
                        </button>
                      ) : (
                        <button
                          className="green"
                          onClick={() => menQuantity(idx)}
                        >
                          -{" "}
                        </button>
                      )}
                      <button className="ss">{daftar.quantity}</button>
                      {daftar.quantity == 45 ? (
                        <button
                          className="grey"
                          onClick={() => plusQuantity(idx)}
                        >
                          {" "}
                          +
                        </button>
                      ) : (
                        <button
                          className="green"
                          onClick={() => plusQuantity(idx)}
                        >
                          {" "}
                          +
                        </button>
                      )}
                    </h6>
                  </div>
                  {localStorage.getItem("id") !== null ? (
                    <div>
                      <Button
                        className="bell"
                        variant="success"
                        type="button"
                        onClick={() => addToCart(daftar)}
                      >
                        Buy
                      </Button>
                    </div>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            ))}
          </div>
        )}

        <ReactPaginate
          previousLabel={"Previous"}
          nextLabel={"Next"}
          breakLabel={"._."}
          pageCount={pages}
          //  marginPagesDisplayed={page}s
          //  pageRangeDisplayed={3}
          onPageChange={(e) => getAll(e.selected)}
          containerClassName={"pagination justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
    </div>
  );
}
