import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import Swal from "sweetalert2";
import "../style/homeA.css";

export default function HomeAdmin() {
  const [menu, setMenu] = useState([]);
  const [pages, setPages] = useState(0);
  const [nama, setNama] = useState ("");

  const getAll = async (idx) => {
    await axios
      .get(`http://localhost:3008/produck/all-produck?nama=${nama}&page=${idx}`)
      .then((res) => {
        setMenu(res.data.data.content);
        setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const deleteUser = async (id) => {
    await Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3008/produck/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
    getAll(0);
  };

  return (
    <div className="adm" style={{ padding: 20 }}>
      <div className="flex-wrap">
        {/* <h2>Welcome Admin</h2>
        <hr /> */}
        <div className="my-5">
          <form class="d-flex" role="search">
            <input
              class="form-control me-2"
              type="search"
              value={nama}
              onChange={(e)=> setNama(e.target.value)}
              placeholder="Search"
              aria-label="Search"
            />
           <button class='btn btn-outline-success' type="button" onClick={() => getAll(0)}>Search</button>
          </form>
          <br />

          {
            menu.length==0? 
            <p className="Noo" align="center">produck tidak tersedia</p>
            :
            <div className="tablee">
                <table className="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama Menu</th>
                <th className="imgs">Image</th>
                <th>Deskripsi</th>
                <th>Harga</th>
                {localStorage.getItem("id") !== null ? (
                  <th className="aksi">Aksi</th>
                ) : (
                  <></>
                )}
              </tr>
            </thead>
            <tbody className="colorr">
              {menu.map((daftar, index) => (
                <tr key={daftar.id}>
                  <td>{index + 1}</td>
                  <td>{daftar.nama}</td>
                  <td>
                    <img
                      src={daftar.img}
                      alt=""
                      style={{ width: 150, height: 100 }}
                    />
                  </td>
                  <td>{daftar.deskripsi}</td>
                  <td>Rp.{daftar.harga}</td>
                  {localStorage.getItem("id") !== null ? (
                    <td>
                      <a href={"/edit/" + daftar.id}>
                        <Button variant="outline-success" className="mx-1">
                          Edit
                        </Button>
                      </a>{" "}
                      <Button
                        variant="outline-danger"
                        className="mx-1"
                        onClick={() => deleteUser(daftar.id)}
                      >
                        Delete
                      </Button>
                    </td>
                  ) : (
                    <></>
                  )}
                </tr>
              ))}
            </tbody>
          </table>

            <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"._."}
              pageCount={pages}
              //  marginPagesDisplayed={page}s
              //  pageRangeDisplayed={3}
              onPageChange={(e) => getAll(e.selected)}
              containerClassName={"pagination justify-content-center"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
              activeClassName={"active"}
            />
          </div>
          }
        </div>
      </div>
    </div>
  );
}
