import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

const EditProf = () => {
    const param = useParams();
    const [img, setImg] = useState("");
    const [nama, setNama] = useState("");
    const [email, setEmail] = useState("");
    const [alamat, setAlamat] = useState("");
    const [noTelpon, setNoTelpon] = useState("");

    const history = useHistory();

    useEffect(() => {
      axios
        .get("http://localhost:3008/loginUser/" + param.id)
        .then((response) => {
          const newMenu = response.data.data;
          setImg(newMenu.img);
          setNama(newMenu.nama);
          setEmail(newMenu.email);
          setAlamat(newMenu.alamat);
          setNoTelpon(newMenu.noTelpon);
        })
        .catch((error) => {
          alert("Terjadi kesalahan Lurr!!! " + error);
        });
    }, []);

    const updateProfil = async (e) => {
      e.persist();
      e.preventDefault();
  
      const fromData = new FormData();
      fromData.append("file", img);    
      fromData.append("nama", nama);    
      fromData.append("email", email);    
      fromData.append("alamat", alamat);  
      fromData.append("noTelpon", noTelpon);  
      
      Swal.fire({
        title: "Yakin Ingin mengubah data ini?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, edit it!",
      })
        .then((result) => {
          if (result.isConfirmed) {
            axios.put(
              "http://localhost:3008/loginUser/" + param.id, fromData
              // {
              //   img: img,
              //   nama: nama,
              //   deskripsi: deskripsi,
              //   harga: Number(harga),
              // },
              // {
              //   headers: {
              //     Authorization: `Bearer ${localStorage.getItem("token")}`,
              //   },
              // }
            );
          }
        })
        .then(() => {
          history.push("/profil");
          Swal.fire("Berhasil!!", "Data kamu berhasil di update,", "success");
          setTimeout(() => {
           
          }, 1500);
        })
        .catch((error) => {
          alert("Terjadinya kesalahan: " + error);
        });
    };

    return (
        <div>
            <div className="container my-5">
        <Form onSubmit={updateProfil}>
          <h2>Update Profile :</h2>
          <hr />
          <div className="name mb-3">
            <Form.Label>
              <strong>Image</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Masukkan File"
                type="file"
                accept="image/*"
                onChange={(e) => setImg(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Nama </strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Nama "
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Email</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Alamat</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="alamat"
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>No Telpon</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="float"
                placeholder="0"
                value={noTelpon}
                onChange={(e) => setNoTelpon(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-2">
            <button className="btn btn-primary" type="submit">
              Save
            </button>
          </div>
        </Form>
      </div>
        </div>
    );
}

export default EditProf;
