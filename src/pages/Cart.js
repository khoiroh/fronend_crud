import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import ReactPaginate from "react-paginate";
import Swal from "sweetalert2";
import "../style/cart.css";

export default function Cart() {
  const [menu, setMenu] = useState([]);
  const [pages, setPages] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalData, setTotalData] = useState([]);

  const getAll = async (page = 0) => {
    const res = await axios
    .get(`http://localhost:3008/cart?page=0&userId=${localStorage.getItem("id")}`);
    console.log(res.data.data.content);
    setMenu(res.data.data.content);
    setPages(res.data.data.totalPages);
    let total = 0;
    res.data.data.content.map((item) => {
      total += item.totalPrice;
    });
    setTotalPrice(total);
  };

  // const changeChecked = (idx) => {
  //   const data = [...totalData];
  //   data[idx].checked = !data[idx].checked;
  //   setListData(data);
  // };

  const konversionRp = (angka) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(angka);
  };
  useEffect(() => {
    getAll();
  }, []);

  const deleteA = async (id) => {
    await Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("http://localhost:3008/cart/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        getAll();
      }
    });
  };

  const checkout = async () => {
    await axios.delete(`http://localhost:3008/cart/checkout?userId=${localStorage.getItem("id")}`);
    Swal.fire({
      icon: "success",
      title: "yeyy Berhasil Checkout !!!",
      timer: 5000,
      buttons: false,
    });
    getAll();
  };
  return (
    <div className="keranjang">
      <div className="cart">
        <h2>
          <i class="fas fa-cart-plus"></i>
        </h2>
        <table className="table ">
          <thead>
            <tr>
              <th>No.</th>
              <th>Nama Menu</th>
              <th>Image</th>
              <th>Deskripsi</th>
              <th>Jumlah</th>
              <th>Harga</th>
              {localStorage.getItem("id") !== null ? <th>Action</th> : <></>}
            </tr>
          </thead>
          <tbody>
            {menu.map((daftar, index) => (
              <tr key={daftar.id} style={{color: "black", textAlign: "center"}}>
                <td>{index + 1}</td>
                <td>{daftar.produckId.nama}</td>
                <td>
                  <img
                    src={daftar.produckId.img}
                    alt=""
                    style={{ width: 150, height: 100 }}
                  />
                </td>
                <td>{daftar.produckId.deskripsi}</td>
                <td className="qty">{daftar.quantity}</td>
                <td>{konversionRp(daftar.totalPrice)}</td>
                {localStorage.getItem("id") !== null ? (
                  <td>
                    <Button
                      variant="danger"
                      className="mx-1"
                      onClick={() => deleteA(daftar.id)}
                    >
                      Hapus
                    </Button>
                  </td>
                ) : (
                  <></>
                )}
              </tr>
            ))}
          </tbody>
        </table>

        {/* <ReactPaginate
              previousLabel={"Previous"}
              nextLabel={"Next"}
              breakLabel={"._."}
              pageCount={pages}
              //  marginPagesDisplayed={page}s
              //  pageRangeDisplayed={3}
              onPageChange={(e) => getAll(e.selected)}
              containerClassName={"pagination justify-content-center"}
              pageClassName={"page-item"}
              pageLinkClassName={"page-link"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextClassName={"page-item"}
              nextLinkClassName={"page-link"}
              activeClassName={"active"}
            /> */}

        <strong>Total Harga : {konversionRp(totalPrice)}</strong>
        <div>
          <Button variant="warning" onClick={() => checkout()}>
            Checkout
          </Button>
        </div>
        <br />
        <div className="kembali">
          <a href="/home">
            <h2>
              <b>
                <i class="fas fa-arrow-alt-circle-left"></i>
              </b>
            </h2>
          </a>
        </div>
      </div>
    </div>
  );
}
