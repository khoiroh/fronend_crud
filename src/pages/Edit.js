import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/edit.css";

export default function Edit() {
  const param = useParams();
  const [img, setImg] = useState("");
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");

  const history = useHistory();

  const updateList = async (e) => {
    e.persist();
    e.preventDefault();

    const fromData = new FormData();
    fromData.append("file", img);    
    fromData.append("nama", nama);    
    fromData.append("deskripsi", deskripsi);    
    fromData.append("harga", harga);  
    
    Swal.fire({
      title: "Yakin Ingin mengubah data ini?",
      text: "Anda tidak akan dapat mengembalikan data ini!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, edit it!",
    })
      .then((result) => {
        if (result.isConfirmed) {
          axios.put(
            "http://localhost:3008/produck/" + param.id, fromData
            // {
            //   img: img,
            //   nama: nama,
            //   deskripsi: deskripsi,
            //   harga: Number(harga),
            // },
            // {
            //   headers: {
            //     Authorization: `Bearer ${localStorage.getItem("token")}`,
            //   },
            // }
          );
        }
      })
      .then(() => {
        history.push("/homeAdmin");
        Swal.fire("Berhasil!!", "Data kamu berhasil di update,", "success");
        setTimeout(() => {
         
        }, 1500);
      })
      .catch((error) => {
        alert("Terjadinya kesalahan: " + error);
      });
  };

  useEffect(() => {
    axios
      .get("http://localhost:3008/produck/" + param.id)
      .then((response) => {
        const newMenu = response.data.data;
        setImg(newMenu.img);
        setNama(newMenu.nama);
        setDeskripsi(newMenu.deskripsi);
        setHarga(newMenu.harga);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Lurr!!! " + error);
      });
  }, []);

  // const submitActionHalder = async (event) => {
  //   event.preventDefault();

  //   await Swal.fire({
  //     title: "Yakin Ingin mengubah data ini?",
  //     text: "You won't be able to revert this!",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Yes, edit it!",
  //   })
  //     .then((result) => {
  //       if (result.isConfirmed) {
  //         axios.put(
  //           "http://localhost:3008/produck/" + param.id,
  //           {
  //             img: img,
  //             nama: nama,
  //             deskripsi: deskripsi,
  //             harga: Number(harga),
  //           },
  //           {
  //             headers: {
  //               Authorization: `Bearer ${localStorage.getItem("token")}`,
  //             },
  //           }
  //         );
  //       }
  //     })
  //     .then(() => {
  //       history.push("/homeAdmin");
  //       Swal.fire("Berhasil!!", "Data kamu berhasil di update,", "success");
  //       setTimeout(() => {
         
  //       }, 1500);
  //     })
  //     .catch((error) => {
  //       alert("Terjadinya kesalahan: " + error);
  //     });
  // };
  return (
    <div className="edit mx-5">
      <div style={{padding: 10, backgroundColor: "gray"}} className="container my-5">
        <div className="eddit">
        <Form onSubmit={updateList}>
          <h2>Update Data :</h2>
          <hr />
          <div style={{display: "flex"}}>
          <img src="https://cdn-icons-png.flaticon.com/512/6550/6550997.png" alt="" width={250} />
          <div style={{marginTop: 30}}>
          <div className="name mb-3">
            <Form.Label>
              <strong>Image</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Masukkan Link Gambar"
                onChange={(e) => setImg(e.target.files[0])}
                type="file"
                accept="image/*"
            />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Nama </strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Nama "
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Deskripsi</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Deskripsi"
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Harga</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="float"
                placeholder="Harga"
                value={harga}
                onChange={(e) => setHarga(e.target.value)}
              />
            </InputGroup>
          </div></div>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-2">
            <button style={{width:600}} className="btn btn-primary" type="submit">
              Save
            </button>
          </div>
        </Form></div>
      </div>
    </div>
  );
}
