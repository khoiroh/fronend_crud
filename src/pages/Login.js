import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // const [fotoProfil, setFotoProfil] = useState("");

  const history = useHistory();

  // const login = async (e) => {
  //   e.preventDefault();
  //   axios.get("http://localhost:3008/loginUser/sign-in").then(({ data }) => {
  //     const user = data.find(
  //       (x) => x.email === email && x.password === password
  //     );
  //     if (user) {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Masuk Sebagai " + email,
  //         timer: 5000,
  //         buttons: false,
  //       });
  //       localStorage.setItem("id", user.id);
  //       history.push("/home");
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 1500);
  //     } else {
  //       Swal.fire({
  //         icon: "error",
  //         title: "Email / Password tidak valid",
  //         timer: 5000,
  //         buttons: false,
  //       });
  //     }
  //   });
  // };

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:3008/loginUser/sign-in",
        {
          email: email,
          password: password,
          // fotoProfil: fotoProfil,
        }
      );
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login berhasil!!!",
          showCancelButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.user.role);
        history.push("/home");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showCancelButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <div className="loog">
      <div style={{backgroundColor: "gray"}} className="container">
        <div
          style={{
            backgroundColor: "black",
            borderRadius: 10,
            padding: 5,
            paddingTop: 10,
            color: "skyblue",
          }}
        >
          <div className="mass">
            <h2 style={{textAlign: "center"}}>
              Form User
            </h2>
            <h4 >
              <a className="buat" href="/register"><i class="fas fa-user-edit"></i></a> 
            </h4>
          </div><hr />
          <Form onSubmit={login} method="POST">
            <div className="fott">
              <img
                src="https://cdn-icons-png.flaticon.com/512/6073/6073874.png"
                alt=""
                width={200}
              />
              <div style={{ marginTop: 20 }}>
                <div className="">
                  <Form.Label>
                    <strong>Email :</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control style={{backgroundColor: "silver"}}
                      placeholder="Email"
                      type="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </InputGroup>
                </div>

                <div className="">
                  <Form.Label>
                    <strong>Password :</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control style={{backgroundColor: "silver"}}
                      placeholder="Password"
                      type="password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </InputGroup>
                </div>
              </div>
            </div>

            {/* <div className="mb-3">
          <Form.Label>
            <strong>File</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="file"
              type="file"
              // value={file}
              onChange={(e) => setFotoProfil(e.target.value)}
            />
          </InputGroup>
        </div> */}

            <button
              type="submit"
              className="btn btn-primary"
              style={{ width: 550, marginLeft: 5 }}
            >
              Login
            </button>
            <br />
            <br />

            {/* <span>Jika Belum Memiliki Akun, Masuk sebagai </span>
        <a href="/register"> <i> <b>Register</b></i></a> */}
          </Form>
        </div>
      </div>
    </div>
  );
}

export default Login;
