import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css";

function LoginAdmin() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try{
      const{data, status } = await axios.post(
        "http://localhost:3008/loginUser/sign-in",
        {
          email: email,
          password: password,
        }
      );
      if(status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login berhasil!!!",
          showCancelButton: false,
          timer:1500,
        });
        localStorage.setItem("id", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.user.role);
        history.push("/homeAdmin");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showCancelButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  // const [LoginAdmin, setLoginAdmin] = useState({
  //   email: '',
  //   password: '',
  //   role: 'ADMIN' ,
  // });
  
  return (
    <div className="container border my-5 pt-3 pb-5 px-5">
      <h1 className="mb-5">Form Admin</h1>
      <Form onSubmit={login} method="POST">
      <div className="mb-3">
          <Form.Label>
            <strong>Email</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="Email"
              type="text"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </InputGroup>
        </div>

        <div className="mb-3">
          <Form.Label>
            <strong>Password</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </InputGroup>
        </div>

        <button type="submit" className="mx-1 btn btn-primary">
          Login
        </button>
        <br />
        <br />

        <span>Masuk Sebagai</span>
        <a href="/">
          {" "}
          <i>
            {" "}
            <b>User</b>
          </i>
        </a>
      </Form>
    </div>
  );
}

export default LoginAdmin;
