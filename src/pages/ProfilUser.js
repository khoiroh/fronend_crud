import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/profil.css";

export default function () {
  const [profil, setProfil] = useState({
    fotoProfil: null,
    nama: "",
    alamat: "",
    email: "",
    noTelpon: "",
    password: "",
  });
  const [show, setShow] = useState(false);
  const [fotoProfil, setFotoProfil] = useState("");
  const [nama, setNama] = useState("");
  const [email, setEmail] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTelpon, setNoTelpon] = useState("");
  const [password, setPassword] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // const history = useHistory();

  useEffect(() => {
    axios
      .get("http://localhost:3008/loginUser/" + localStorage.getItem("id"))
      .then((response) => {
        const profil = response.data.data;
        setFotoProfil(profil.fotoProfil);
        setNama(profil.nama);
        setEmail(profil.email);
        setAlamat(profil.alamat);
        setNoTelpon(profil.noTelpon);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Lurr!!! " + error);
      });
  }, []);

  const updateProfil = async (e) => {
    e.persist();
    e.preventDefault();

    const fromData = new FormData();
    fromData.append("file", fotoProfil);    
    fromData.append("nama", nama);    
    fromData.append("email", email);    
    fromData.append("alamat", alamat);  
    fromData.append("noTelpon", noTelpon);  
    fromData.append("password", password);  
    
    try {
      await axios.put(
        `http://localhost:3008/loginUser/${localStorage.getItem("id")}`, fromData
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit profile",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async () => {
    await axios
      .get("http://localhost:3008/loginUser/" + localStorage.getItem("id"))
      .then((res) => {
        setProfil(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  return (
    <div className="prof">
      <div className="pati">
        <div className="hits">
          {/* <h5 className="h5">Profil Saya</h5>
          <p>
            Kelola informasi profil Anda untuk mengontrol, melindungi dan
            mengamankan akun
          </p>
          <hr /> */}
          <div className="cil" style={{ display: "flex", gap: 35 }}>
            <div style={{padding: 15, backgroundColor: "grey", borderRadius: 10}}>
            <div
              class="card"
              style={{
                width: "50rem",
                height: "25rem",
                display: "flex",
                backgroundColor: "black",
                padding: 10,
                color: "silver",
              }}
            >
              <div style={{}}>
                <h5 class="card-title">{profil.nama}</h5>
              </div><hr />
                <div style={{display: "flex", gap: 100, padding: 20}}>
                <div>
              <img
                className="gamb"
                src={profil.fotoProfil}
                alt=""
                width={250}
                height={200}
                type="file"
              />
            </div>
              <div className="text" >
                <h5 class="card-text"><i class="fas fa-envelope-open"></i> { profil.email}</h5><br />
                <h5 class="card-text"><i class="fas fa-map-marker-alt"></i> { profil.alamat}</h5><br />
                <h5 class="card-text"><i class="fas fa-phone-volume"></i> { profil.noTelpon}</h5>                
                {/* <h6 class="card-text">Password : {profil.password}</h6>                 */}
              </div>
              
              </div>
              {/* {localStorage.getItem("id") !== null ? (
                  <div className="prIn">
                    <a href={"/editP/" + profil.id}>
                      <Button variant="outline-success" className="mx-1">
                        Edit
                      </Button>
                    </a>
                  </div>
                ) : (
                  <></>
                )} */}
                <div className="prIn" >
                <Button variant="outline-success" onClick={handleShow} type="button" className="mx1">Edit</Button>
                </div>
            </div></div>
          </div>
        </div>
      </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="dal" closeButton>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={updateProfil}>
            <div className="mb-3">
              <Form.Label>
                <strong>Profile</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan Link"
                  onChange={(e) => setFotoProfil(e.target.files[0])}
                  type="file"
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Nama</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan Menu "
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="email"
                  placeholder="Massukan Deskripsi"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Alamat</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan alamat"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>NoTelpon</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="no tlpn"
                  value={noTelpon}
                  onChange={(e) => setNoTelpon(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                type="password"
                  placeholder="Massukan password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </InputGroup>
            </div>
            <button className="btn btn-danger" onClick={handleClose}>
              Close
            </button>
            ||
            <button
              className="btn btn-primary"
              type="submit"
              onClick={handleClose}
            >
              Save
            </button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
// https://qph.cf2.quoracdn.net/main-qimg-a80f3491ad8b891a98187748d6811a0d-pjlq
