// import axios from 'axios';
import axios from 'axios';
import React, { useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import "../style/register.css"
// import Swal from 'sweetalert2';

export default function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [nama, setNama] = useState("");
    const [alamat, setAlamat] = useState("");
    const [noTelpon, setNoTelpon] = useState("");
    const [role, setRole] = useState("");
    const [fotoProfil, setFotoProfil] = useState(null);
  
    const register = async (e) => {
      e.preventDefault();
      e.persist();
  

    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("noTelpon", noTelpon);
    formData.append("file", fotoProfil);
    formData.append("role" , "USER");

    try {
      await axios.post("http://localhost:3008/loginUser", formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
      });
      Swal.fire({
        icon: "success",
        title: "Register berhasil!!!",
        showCancelButton: false,
        timer:1500,
      });
      setTimeout(() => {
        window.location.reload();
        history.push('/')
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };


const history = useHistory()

// const [userRegister, setUserRegister] = useState({
//   email: '',
//   password: '',
//   nama: '',
//   alamat: '',
//   noTelepon: '',
//   role: 'USER' ,
// })

// const handleOnChange = (e) => {
//   setUserRegister((currUser) => {
//     return { ...currUser, [e.target.id]: e.target.value}
//   })
// }
// const register = async (e) => {
// e.preventDefault()
// try {
//   const response = await fetch("http://localhost:3008/loginUser/", {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify(userRegister),
//   })
//   if(response.ok) {
//     alert('register succes')
//     setTimeout(() => {
      // history.push('/')
//     },1250)
//   }
// }catch(err) {
//   console.log(err)
// }
// }
return (
    <div style={{paddingTop: 10, paddingBottom: 10}}>
         <div style={{width: 650}} className="container">
          <div className='ress'>
          <div style={{display: "flex", gap: 30,padding: 5}}>
          <img src="https://cdn-icons-png.flaticon.com/512/3820/3820241.png" alt="" width={200} height={150}/>
          <div style={{display: "flex", gap: 20, marginTop: 30}}>
          <h1 className="mb-5">Daftar Akun</h1>
      <h4 style={{marginTop: 50}}>
              <a className="logg" href="/"><i class="fas fa-sign-in-alt"></i></a> 
            </h4>
          </div>
          </div>
      <Form onSubmit={register} method="POST">
        <div className="mMm">
          <Form.Label>
            <strong>Email</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control 
              placeholder="Masukkan Email"
              id='email'
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              
              required
            />
          </InputGroup>
        </div>

        <div className="mMm">
          <Form.Label>
            <strong>Password</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control className='nNn'
              placeholder="Password"
              type="password"
              id='password'
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </InputGroup>
        </div>

        <div className="mMm">
          <Form.Label>
            <strong>Nama</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="Nama"
              type="nama"
              id='nama'
              value={nama}
              onChange={(e) => setNama(e.target.value)}
              required
            />
          </InputGroup>
        </div>

        <div className="mMm">
          <Form.Label>
            <strong>Alamat</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="Alamat"
              type="alamat"
              id='alamat'
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
              required
            />
          </InputGroup>
        </div>

        <div className="mMm">
          <Form.Label>
            <strong>No Telepon</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="masukkan angka"
              type="no telepon"
              id='no telepon'
              value={noTelpon}
              onChange={(e) => setNoTelpon(e.target.value)}
              required
            />
          </InputGroup>
        </div>

        <div className="mMm">
          <Form.Label>
            <strong>File</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              placeholder="file"
              type="file"
              file={fotoProfil}
              onChange={(e) => setFotoProfil(e.target.files[0])}
            />
          </InputGroup>
        </div>

        
      </Form>
        <button type="submit" className="batt">
          <b>
          Daftar
          </b>
        </button>
    </div></div>
    </div>
  )
}
