import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/Nav.css";
import image from "../image/prof-home.png";

export default function NavigationBar() {
  const [show, setShow] = useState(false);
  const [img, setImg] = useState("");
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // const addUser = async (e) => {
  //   e.preventDefault();

  //   const data = {
  //     link: link,
  //     namaMakanan: namaMakanan,
  //     deskripsi: deskripsi,
  //     harga: harga,
  //   };

  //   await axios.post("http://localhost:3008/loginUser/sign-up", data);
  //   Swal.fire("Good job!", "You clicked the button!", "success")
  //     .then(() => {
  //       window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan " + error);
  //     });
  // };

  const history = useHistory();

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/");
  };

  const addUser = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", img);
    formData.append("nama", nama);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    try {
      await axios.post("http://localhost:3008/produck", formData, {
        headers: {
          Authorization: `  Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "multipart/form-data",
        },
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Menambahkan Data!!!",
        showCancelButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <nav className="navbar navbar-expand-lg ">
        <div className="container-fluid">
            <img className="image" src={image} alt="" />
          <a className="NN navbar-brand" href="/home">
            <b>Warung </b>
            <i class="fas fa-store-alt"></i>
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="/homeAdmin"
                >
                  <i class="fas fa-home"></i>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="/profil"
                >
                  <i class="fas fa-user"></i>
                </a>
              </li>

              {localStorage.getItem("role") === "ADMIN" ? (
                <>
                  <li className="nav-item">
                    <button
                      className="btn btn-outline-warning"
                      onClick={handleShow}
                    >
                      <b>Tambah Menu</b>
                    </button>
                  </li>
                </>
              ) : (
                <></>
              )}
            </ul>

            {localStorage.getItem("id") !== null ? (
                <div className="nav-item float-right">
                  <a
                    className="nav-link active"
                    aria-current="page"
                    href="/cart"
                  >
                    <i class="fas fa-cart-plus"></i>
                  </a>
                </div>
              ) : (
                <></>
              )}
            {/* <form class="d-flex" role="search"> */}
            {localStorage.getItem("id") !== null ? (
              <>
                <div className="nav-item float-right">
                  <a className="btn" onClick={logout}>
                    <i class="fas fa-sign-out-alt"></i> logout
                  </a>
                </div>
              </>
            ) : (
              <div className="nav-item float-right">
                <a className="btn" href="/">
                  <i class="fas fa-sign-in-alt"></i> login
                </a>
              </div>
            )}
            {/* </form> */}
          </div>
        </div>
      </nav>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="daldal" closeButton>
          <Modal.Title>Add User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addUser}>
            <div className="mb-3">
              <Form.Label>
                <strong>Image</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan Link"
                  onChange={(e) => setImg(e.target.files[0])}
                  type="file"
                  // value={img}
                  // onChange={(e) => setImg(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Nama Menu</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan Menu "
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="deskripsi"
                  placeholder="Massukan Deskripsi"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Harga</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Massukan Harga"
                  value={harga}
                  onChange={(e) => setHarga(e.target.value)}
                />
              </InputGroup>
            </div>
            <button className="btn btn-danger" onClick={handleClose}>
              Close
            </button>
            ||
            <button
              className="btn btn-primary"
              type="submit"
              onClick={handleClose}
            >
              Save
            </button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
